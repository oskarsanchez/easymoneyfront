//const CellsPage = customElements.get('cells-page');
class AccountPage extends CellsPage {
  static get is() {
    return 'account-page';
  }

  static get properties() {
    return {
      user: {
        type: Object,
        value: {}
      },
      error: {
        type: Boolean,
        value: false
      },
      msg: {
        type: String,
        value: "Parece que estamos teniendo problemas en nuestros servidores, intentalo más tarde."
      },
      loading: {
        type: Boolean,
        value : false
      }
    };
  }

  onPageEnter() {
  }

  onPageLeave() {
    this.error = false;
    this.loading = false;
  }

  ready () {
    super.ready();
    this.subscribe('ch_user', (user) => {
      this.user = user;
    });

  }

  _handleLogoutUser(evt) {
    this.set('user',null);
    this.publish('ch_user',null);
    this.publish('ch_account',null);
    this.publish('ch_accounts',null);
    this.navigate('home');
  }

  _handleLogoutUserError(evt) {
    this.error = true;
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      this.msg = detail.msg;
    }
  }
  _handleEditUser(evt) {
    this.navigate('user');
  }

  _handleAddAccountOk(evt) {
    this.navigate('accounts');
  }

  _handleAddAccountError(evt) {
    this.error = true;
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      this.msg = detail.msg;
    }
  }

  _handleAccountCancel(evt) {
    this.navigate('accounts');
  }

  _handleNotificationButtonClicked(evt){
    this.error = false;
  }
}
window.customElements.define(AccountPage.is, AccountPage);
