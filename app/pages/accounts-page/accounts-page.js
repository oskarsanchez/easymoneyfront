//const CellsPage = customElements.get('cells-page');
class AccountsPage extends CellsPage {
  static get is() {
    return 'accounts-page';
  }

  static get properties() {
    return {
      user : {
        type: Object,
        value: {}
      },
      accounts : {
        type: Array,
        value: []
      },
      error:{
        type: Boolean,
        value: false
      },
      msg:{
        type: String,
        value: "Parece que estamos teniendo problemas en nuestros servidores, intentalo más tarde."
      },
      loading:{
        type: Boolean,
        value: false
      }
    };
  }

  onPageEnter() {
    this.$.accountsdp.headers = {
      authorization : "JWT " + sessionStorage.getItem("token"),
      user_id : parseInt(sessionStorage.getItem("userId"))
    };
    this.loading = true;
    this.error = false;
    this.$.accountsdp.host = cells.urlEasyMoneyBankBankServices;
    this.$.accountsdp.generateRequest();
  }

  onPageLeave() {
    this.error = false;
    this.loading = false;
    this.$.productitemlist.items = [];
  }

  ready() {
    super.ready();
    this.accounts = null;
    this.subscribe('ch_user', (user) => {
      this.user = user;
    });
  }

  _handleLogoutUser(evt) {
    this.error = false;
    this.set('user',null);
    this.set('accounts',null);
    this.publish('ch_user',null);
    this.publish('ch_account',null);
    this.publish('ch_accounts',null);
    this.navigate('home');
  }

  _handleLogoutUserError(evt) {
    this.error = true;
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      this.msg = detail.msg;
    }
  }

  _handleEditUser(evt) {
    this.navigate('user');
  }

  _handleRequestAccountsSuccess(evt) {
    const detail = evt.detail;
    delete detail.headers;
    this.set('accounts',detail);
    this.publish('ch_accounts', this.accounts);
    let accountsItemList = [];
    detail.forEach(
      function (account){
        let accountItem = {};
        accountItem.name = account.type;
        accountItem.description = {'value': account.iban, 'masked': false};
        accountItem.primaryAmount = {'label': 'Disponible', 'amount': account.balance, 'currency': 'EUR'};
        accountItem.id = account.id;
        accountItem.imgSrc = cells.uriEndpointWeb + "/resources/images/imgCuentaPersonal.png";
        accountsItemList.push(accountItem);
      }
    );
    this.loading = false;
    this.$.productitemlist.items = accountsItemList;
  }

  _handleRequestAccountsError(evt) {
    this.loading = false;
    const detail = evt.detail;
    if (detail && detail.status != 404) {
        this.msg = detail.msg;
        this.error = true;
    }
  }

  _handleAddAccountClick(evt) {
    this.navigate('account');
  }

  _handleProductItemTap(evt) { 
    const detail = evt.detail;
    delete detail.headers;
    let index = this.accounts.findIndex(
      function(element){
        if (element.id == detail.productId)
          return element;
      }
    );
    this.publish('ch_account', this.accounts[index]);
    this.navigate('movements');
  }

  _handleNotificationButtonClicked(evt) {
    this.error = false;
  }

}
window.customElements.define(AccountsPage.is, AccountsPage);
