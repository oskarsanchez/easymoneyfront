//const CellsPage = customElements.get('cells-page');
class ForgotPage extends CellsPage {
  static get is() {
    return 'forgot-page';
  }

  static get properties() {
    return {
      error: {
        type: Boolean,
        value: false
      },
      msg: {
        type: String,
        value: "Parece que estamos teniendo problemas en nuestros servidores, intentalo más tarde."
      },
      loading: {
        type: Boolean,
        value : false
      }
    };
  }

  onPageEnter() {
   
  }

  onPageLeave() {
    this.error = false;
    this.loading = false;
  }

  ready () {
    super.ready();
  }

  _handleLogoutUser(evt) {
    this.set('user',null);
    this.publish('ch_user',null);
    this.publish('ch_account',null);
    this.publish('ch_accounts',null);
    this.navigate('home');
  }

  _handleLogoutUserError(evt) {
    this.error = true;
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      this.msg = detail.msg;
    }
  }

  _handleEditUser(evt) {
    
  }

  _handleForgotOk(evt) {
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      this.msg = detail.msg;
    }
    this.loading = false;
    this.navigate('home');
  }

  _handleForgotError(evt) {
    this.error = true;
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      this.msg = detail.msg;
    }
    this.loading = false;
  }

  _handleForgotCancel(evt) {
    this.navigate('login');
  }

  _handleNotificationButtonClicked(evt){
    this.error = false;
  }
}
window.customElements.define(ForgotPage.is, ForgotPage);