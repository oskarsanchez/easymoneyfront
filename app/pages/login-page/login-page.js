const CellsPage = customElements.get('cells-page');
class LoginPage extends CellsPage {
  static get is() {
    return 'login-page';
  }

  static get properties() {
    return {
      user: {
        type: Object
      },
      error:{
        type: Boolean,
        value: false
      },
      msg: {
        type: String,
        value: "Parece que estamos teniendo problemas en nuestros servidores, intentalo más tarde."
      },
      loading: {
        type: Boolean,
        value : false
      }
    };
  }

  onPageEnter() {
    let userStorage = localStorage.getItem("userData");
    if (userStorage){
      let userData = JSON.parse(userStorage);
      this.$.loginform.userName = userData.firstName;
      this.$.loginform.userId = userData.email;
    }
    this.error = false;
  }

  onPageLeave() {
    this.$.loginform.userPassword = "";
    this.$.loginform.userName = "";
    this.$.loginform.userId = "";
    this.loading = false;
    this.error = false;
  }

  ready() {
    super.ready();
    let userData = ((localStorage.getItem("userData") != undefined) && (localStorage.getItem("userData") != ""))
                    ? JSON.parse(localStorage.getItem("userData")) : undefined;
    if (userData){
      this.$.loginform.userName = userData.firstName;
      this.$.loginform.userId = userData.email;
    }
  }

  _handleLogin(evt) {
    this.loading = true;
    let bodyLogin = {};
    bodyLogin.email = this.$.loginform.userId;
    bodyLogin.password = this.$.loginform.userPassword;
    this.$.logindp.body = bodyLogin;
    this.error = false;
    this.$.logindp.host = cells.urlEasyMoneyBankAuthServices;
    this.$.logindp.generateRequest();
  }

  _handleRegister(evt) {
    this.navigate('user');
  }

  _handlerPasswordRecover(evt) {
    this.navigate('forgot');
  }

  _handleChangeUser(evt) {
    this.$.loginform.userName = "";
    this.$.loginform.userId = "";
    this.$.loginform.userPassword = "";
    localStorage.setItem("userData","");
    sessionStorage.setItem("token","");
    sessionStorage.setItem("userId","");
  }

  _handleRequestSuccess(evt) {
    const detail = evt.detail;
    delete detail.headers;
    const normalizedUser = {
      id : detail.id,
      firstName: detail.firstName,
      lastName : detail.lastName,
      email: detail.email,
      gender: detail.gender,
      avatar: detail.avatar
    };
    localStorage.setItem("userData",JSON.stringify(normalizedUser));
    sessionStorage.setItem("token",detail.token);
    sessionStorage.setItem("userId",detail.id);
    this.set('user',normalizedUser);
    this.publish('ch_user',normalizedUser);
    this.loading = false;
    this.navigate('accounts');
  }

  _handleRequestError(evt) {
    this.error = true;
    this.loading = false;
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      this.msg = detail.msg;
    }
  }

  _handleBack(){
    this.navigate('home');
  }

  _handleNotificationButtonClicked(evt){
    this.error = false;
  }
}
window.customElements.define(LoginPage.is, LoginPage);