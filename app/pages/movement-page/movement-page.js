//const CellsPage = customElements.get('cells-page');
class MovementPage extends CellsPage {
  static get is() {
    return 'movement-page';
  }

  static get properties() {
    return {
      user : {
        type: Object
      },
      account : {
        type: Object
      },
      error: {
        type: Boolean,
        value: false
      },
      msg: {
        type: String,
        value: "Parece que estamos teniendo problemas en nuestros servidores, intentalo más tarde."
      },
      loading: {
        type: Boolean,
        value : false
      }
    };
  }

  onPageEnter() {
  }

  onPageLeave() {
    this.error = false;
    this.loading = false;
    this.$.easymoneymovementform.reset();
  }

  ready() {
    super.ready();
    this.subscribe('ch_user', (user) => {
      this.user = user;
    });
    this.subscribe('ch_account', (account) => {
      this.account = account;
    });
  }

  _handleLogoutUser(evt) {
    this.set('user',null);
    this.set('account',null);
    this.publish('ch_user',null);
    this.publish('ch_account',null);
    this.publish('ch_accounts',null);
    this.navigate('home');
  }

  _handleLogoutUserError(evt) {
    this.error = true;
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      this.msg = detail.msg;
    }
  }

  _handleEditUser(evt) {
    this.navigate('user');
  }

  _handleAddMovementOk(evt) {
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      this.msg = detail.msg;
      this.account.balance = detail.accountBalance;
    }
    this.set('account',this.account);
    this.publish('ch_account', this.account);
    this.loading = false;
    this.navigate('movements');
  }

  _handleAddMovementError(evt) {
    this.error = true;
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      this.msg = detail.msg;
    }
    this.loading = false;
  }

  _handleMovementCancel(evt) {
    this.navigate('movements');
  }

  _handleNotificationButtonClicked(evt){
    this.error = false;
  }
}
window.customElements.define(MovementPage.is, MovementPage);