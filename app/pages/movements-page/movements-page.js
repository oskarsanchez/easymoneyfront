//const CellsPage = customElements.get('cells-page');

class MovementsPage extends CellsPage {
  static get is() {
    return 'movements-page';
  }
  static get properties() {
    return {
      user : {
        type: Object,
        value: {}
      },
      account : {
        type: Object,
        value: {},
        notify: true
      },
      movements : {
        type: Array,
        value: []
      },
      _account: {
        type: Object,
        computed: '_computeAccount(account)',
        notify: true
      },
      operations:{
        type: Array,
        value: [{
                  label: 'Transferencia',
                  id: 'transfer',
                  icon: 'coronita:transfer'
                },
                {
                  label: 'Buscar Movimientos',
                  id: 'find-movements',
                  icon: 'coronita:search'
                },
                {
                  label: 'Ingreso/Reintegro',
                  id: 'movement',
                  icon: 'coronita:chargecard'
                },
                {
                  label: 'Info',
                  id: 'info',
                  icon: 'coronita:info'
                }
              ]
      },
      error:{
        type: Boolean,
        value: false
      },
      msg:{
        type: String,
        value: "Parece que estamos teniendo problemas en nuestros servidores, intentalo más tarde."
      },
      loading:{
        type: Boolean,
        value: false
      }
    };
  }

  onPageEnter() {
    this.movements =null;
    this.$.movementslist.items = null;
    this.$.movementsdp.headers = {
      authorization : "JWT " + sessionStorage.getItem("token"),
      user_id : parseInt(sessionStorage.getItem("userId"))
    };
    this.loading = true;
    this.error = false;
    this.$.movementsdp.host = cells.urlEasyMoneyBankBankServices;
    this.$.movementsdp.generateRequest();
  }

  onPageLeave() {
    this.error = false;
    this.loading = false;
    this.$.movementslist.items = null;
    this.account = null;
    this._account = null;
  }

  ready() {
    super.ready();
    this.subscribe('ch_user', (user) => {
      this.set('user',user);
    });
    this.subscribe('ch_account', (account) => {
      this.set('account',account);
    });

  }
  _computeAccount(account) {
    if (account == null)
      return null;
    let accountItem = {};
    accountItem.name = account.type;
    accountItem.description = {'value': account.iban, 'masked': false};
    accountItem.primaryAmount = {'label': 'Disponible', 'amount': account.balance, 'currency': 'EUR'};
    accountItem.id = account.id;
    accountItem.imgSrc =  cells.uriEndpointWeb + "/resources/images/imgCuentaPersonal.png";
    return accountItem;
  }

  _handleLogoutUser(evt) {
    this.error = false;
    this.set('user',null);
    this.set('account',null);
    this.set('movements',null);
    this.publish('ch_user',null);
    this.publish('ch_account',null);
    this.publish('ch_accounts',null);
    this.navigate('home');
  }

  _handleLogoutUserError(evt) {
    this.error = true;
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      this.msg = detail.msg;
    }
  }

  _handleEditUser(evt) {
    this.navigate('user');
  }

  _handleRequestMovementsSuccess(evt) {
    const detail = evt.detail;
    delete detail.headers;
    this.set('movements',detail);
    let movementsItemList = [];
    detail.forEach(
      function (movement){
        let movementItem = {};
        movementItem.date = movement.date.split("/").reverse().join("-");
        movementItem.label = movement.subject;
        movementItem.category = movement.category
        movementItem.categoryDescription = movement.subcategory
        movementItem.categoryIcon = (movement.type == "Ingreso") ? "2" : "9";
        movementItem.parsedAmount = {"value": [[movement.amount]], "currency": [[movement.currency]]};
        if (movement.currency_converted != undefined)
          movementItem.badges = [{
            "status": "default",
            "label": "Cambio de " + movement.amount_converted + " " + movement.currency_converted
          }];
        movementItem.id = movement.id;
        movementsItemList.push(movementItem);
      }
    );
    this.loading = false;
    this.$.movementslist.items = movementsItemList;
  }

  _handleRequestMovementsError(evt) {
    this.loading = false;
    const detail = evt.detail;
    if (detail && detail.status != 404) {
        this.msg = detail.msg;
        this.error = true;
    }
  }

  _handleMovementsListItemTap(evt) {
    const detail = evt.detail;
    delete detail.headers;
  }

  _handleOperationClick(evt) {
    const detail = evt.detail;
    delete detail.headers;
    this.navigate(detail.id);
  }

  _handleNotificationButtonClicked(evt) {
    this.error = false;
  }

  _handleBack(evt){
    this.navigate('accounts');
  }
}
window.customElements.define(MovementsPage.is, MovementsPage);
