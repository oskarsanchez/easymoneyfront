//const CellsPage = customElements.get('cells-page');

class TransferPage extends CellsPage {
  static get is() {
    return 'transfer-page';
  }
  static get properties() {
    return {
      user : {
        type: Object
      },
      account : {
        type: Object
      },
      accounts : {
        type: Array
      },
      error: {
        type: Boolean,
        value: false
      },
      msg: {
        type: String,
        value: "Parece que estamos teniendo problemas en nuestros servidores, intentalo más tarde."
      },
      loading: {
        type: Boolean,
        value : false
      }
    };
  }

  onPageEnter() {
  
  }

  onPageLeave() {
    this.error = false;
    this.loading = false;
    this.$.easymoneytransferform.reset();
  }

  ready() {
    super.ready();
    this.subscribe('ch_user', (user) => {
      this.user = user;
    });
    this.subscribe('ch_account', (account) => {
      this.account = account;
    });
    this.subscribe('ch_accounts', (accounts) => {
      this.accounts = accounts;
    });
  }

  _handleLogoutUser(evt) {
    this.set('user',null);
    this.set('account',null);
    this.set('accounts',null);
    this.publish('ch_user',null);
    this.publish('ch_account',null);
    this.publish('ch_accounts',null);
    this.navigate('home');
  }

  _handleLogoutUserError(evt) {
    this.error = true;
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      this.msg = detail.msg;
    }
  }

  _handleEditUser(evt) {
    this.navigate('user');
  }

  _handleAddTransferOk(evt) {
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      this.msg = detail.msg;
      this.account.balance = detail.accountBalance;
    }
    this.set('account',this.account);
    this.publish('ch_account', this.account);
    this.loading = false;
    this.navigate('movements');
  }

  _handleAddTransferError(evt) {
    this.error = true;
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      this.msg = detail.msg;
    }
    this.loading = false;
  }

  _handleTransferFormCancel(evt) {
    this.navigate('movements');
  }
  _handleNotificationButtonClicked(evt) {
    this.error = false;
  }
}
window.customElements.define(TransferPage.is, TransferPage);
