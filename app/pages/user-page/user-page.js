//const CellsPage = customElements.get('cells-page');
class UserPage extends CellsPage {
  static get is() {
    return 'user-page';
  }

  static get properties() {
    return {
      user : {
        type: Object,
        value: null
      },
      error: {
        type: Boolean,
        value: false
      },
      msg: {
        type: String,
        value: "Parece que estamos teniendo problemas en nuestros servidores, intentalo más tarde."
      },
      loading: {
        type: Boolean,
        value : false
      }
    };
  }

  onPageEnter() {
    
  }

  onPageLeave() {
    this.error = false;
    this.loading = false;
  }

  ready() {
    super.ready();
    this.subscribe('ch_user', (user) => {
      this.user = user;
    });
  }

  _handleLogoutUser(evt) {
    this.set('user',null);
    this.publish('ch_user',null);
    this.publish('ch_account',null);
    this.publish('ch_accounts',null);
    this.navigate('home');
  }

  _handleLogoutUserError(evt) {
    this.error = true;
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      this.msg = detail.msg;
    }
  }

  _handleEditUser(evt) {
    this.navigate('user');
  }

  _handleEditUserOk(evt) {
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      this.msg = detail.msg;
    }
    this.set('user',this.$.easymoneyuserform.user);
    this.publish('ch_user',this.user);
    localStorage.setItem("userData",JSON.stringify(this.user));
    this.loading = false;
    this.navigate('accounts');
  }

  _handleEditUserError(evt) {
    this.error = true;
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      this.msg = detail.msg;
    }
    this.loading = false;
  }

  _handleAddUserOk(evt) {
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      this.msg = detail.msg;
    }
    this.set('user',this.$.easymoneyuserform.user);
    this.publish('ch_user',this.user);
    localStorage.setItem("userData",JSON.stringify(this.user));
    sessionStorage.setItem("token",this.$.easymoneyuserform.token);
    sessionStorage.setItem("userId",this.user.id);
    this.loading = false;
    this.navigate('accounts');
  }

  _handleAddUserError(evt) {
    this.error = true;
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      this.msg = detail.msg;
    }
    this.loading = false;
  }

  _handleEditUserCancel(evt) {
    (this.user == null) ? this.navigate('login')  : this.navigate('accounts');
  }

  _handleNotificationButtonClicked(evt){
    this.error = false;
  }
}
window.customElements.define(UserPage.is, UserPage);
