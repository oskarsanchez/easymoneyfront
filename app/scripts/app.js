(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'home': '/',
      'login': '/login',
      'accounts': '/accounts',
      'account': '/account',
      'user': '/user',
      'movements': '/movements',
      'movement': '/movement',
      'transfer': '/transfer',
      'forgot': '/forgot',
      'changepassword': '/changepassword' 
    }
  });

}(document));
